class AddAssociationsToBook < ActiveRecord::Migration
  def change
    add_reference :books, :category, index: true, foreign_key: true
    add_reference :books, :type, index: true, foreign_key: true
  end
end
