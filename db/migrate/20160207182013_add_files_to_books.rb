class AddFilesToBooks < ActiveRecord::Migration
  def change
    add_column :books, :files, :json
  end
end
