class FrontendController < ApplicationController
  def index
    @books = Book.all
  end

  def show
    @book = Book.find(params[:id])
  end

  def search
    @books = Book.search(params).includes(:category).includes(:type)
  end

  def advanced_search
    @search = Book.ransack(params[:q])
    @books = @search.result
    @search.build_condition if @search.conditions.empty?
    @search.build_sort if @search.sorts.empty?
  end

end
