json.array!(@books) do |book|
  json.extract! book, :id, :title, :author, :year, :cover, :publisher, :isbn, :description
  json.url book_url(book, format: :json)
end
