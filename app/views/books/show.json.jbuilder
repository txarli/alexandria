json.extract! @book, :id, :title, :author, :year, :cover, :publisher, :isbn, :description, :created_at, :updated_at
