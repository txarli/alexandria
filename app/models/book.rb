# == Schema Information
#
# Table name: books
#
#  id          :integer          not null, primary key
#  title       :string
#  author      :string
#  year        :datetime
#  cover       :string
#  publisher   :string
#  isbn        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#  type_id     :integer
#

class Book < ActiveRecord::Base

  # Validates the presence of required fields before proceed to record creation or update
  #
  # == Required fields:
  #
  #   - title
  #   - author
  #   - year
  #   - cover
  #   - publisher
  #   - isbn
  #   - description
  #   - category_id [Association]
  #   - type_id [Association]
  #
  validates_presence_of :title, :author, :year, :cover, :publisher, :isbn, :description, :category_id, :type_id


  # Associations
  #
  #
  belongs_to :category
  belongs_to :type

  # File uploads
  #
  #
  mount_uploaders :files, FileUploader

  # Search books
  #
  # @Param category (string)
  # @Param category_options (string)
  # @Param type ('&&','||')
  # @Param type_options ('&&','||')
  #
  # Return @Hash[Object] with theses attributes:
  #
  #   - id
  #   - title
  #   - author
  #   - year
  #   - cover
  #   - editor
  #   - isbn
  #   - description
  #   - category_id
  #   - type_id
  #   - created_at
  #   - updated_at
  #
  def self.search(params)

    ##
    # Sets order
    books = order(:title)

    ##
    # If both option fields are marked as AND, proceed to make an inclusive query
    if params[:category_options] == "&&" && params[:type_options] == "&&"
      books = books.joins(:category)
                   .where("categories.title" => params[:category])
                   .joins(:type)
                   .where("types.title" => params[:type])

      ##
      # Otherwise, perform an non inclusive OR query
    else
      books = books.joins(:category)
                   .joins(:type)
                   .where('categories.title= ? OR types.title= ?', params[:category], params[:type])
    end

    ##
    # returns book ActiveRecord_Relation
    books

  end


  # Ransack gem searchable attributes for current model
  #
  #
  def self.ransackable_attributes(auth_object = nil)
    %w(title author year publisher isbn description) + _ransackers.keys
  end

end
