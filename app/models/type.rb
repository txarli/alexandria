# == Schema Information
#
# Table name: types
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Type < ActiveRecord::Base

  # Validates the presence of required fields before proceed to record creation or update
  #
  # == Required fields:
  #
  #   - title
  #
  validates_presence_of :title

  # Associations
  #
  #
  has_many :books


  # Ransack gem searchable attributes for current model
  #
  #
  def self.ransackable_attributes(auth_object = nil)
    %w(title) + _ransackers.keys
  end

end
