module FrontendHelper

  def link_to_add_fields(name, f, type)
    new_object = f.object.send "build_#{type}"
    id = "new_#{type}"
    fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
      render(type.to_s + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields btn btn-success btn-xs", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def predicate_conditions
    [:eq, :cont, :cont_any, :cont_all, :not_cont, :not_cont_any, :not_cont_all, :start, :start_any, :start_all, :not_start, :not_start_any, :not_start_all, :end, :end_any, :end_all, :not_end, :not_end_any, :not_end_all, :lt, :lteq, :gt, :gteq]
  end

end
