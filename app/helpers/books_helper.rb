# == Schema Information
#
# Table name: books
#
#  id          :integer          not null, primary key
#  title       :string
#  author      :string
#  year        :datetime
#  cover       :string
#  publisher   :string
#  isbn        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#  type_id     :integer
#

module BooksHelper

  def download_tag(file)
     tag = ""
     tag = "TXT File" if file.url[/txt/]
     tag = "PDF File" if file.url[/pdf/]
     tag = "EPUB File" if file.url[/epub/]
     tag
  end

  def download_button(file)
     button = ""
     button = link_to "<i class='fa fa-download'></i> Download #{download_tag(file)}".html_safe, file.url, class: "btn btn-success"
  end

end
