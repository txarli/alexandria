namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    require 'populator'
    require 'faker'

    [Book, Category, Type].each(&:delete_all)

    Category.populate 30 do |category|
      category.title = Faker::Book.genre
    end

    Type.populate 10 do |type|
      type.title = Faker::Hipster.word
    end

    Book.populate 100..250 do |book|
      book.title = Faker::Book.title
      book.author = Faker::Book.author
      book.year = 50.years.ago..Time.now
      book.cover = "fake-cover.jpg"
      book.publisher = Faker::Book.publisher
      book.isbn = Faker::Code.isbn
      book.description = Faker::Lorem.paragraph(2, false, 40)
      book.category_id = Faker::Number.between(Category.first.id, Category.last.id)
      book.type_id = Faker::Number.between(Type.first.id, Type.last.id)
    end

  end
end