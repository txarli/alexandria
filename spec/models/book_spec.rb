# == Schema Information
#
# Table name: books
#
#  id          :integer          not null, primary key
#  title       :string
#  author      :string
#  year        :datetime
#  cover       :string
#  publisher   :string
#  isbn        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#  type_id     :integer
#

require 'rails_helper'

RSpec.describe Book, type: :model do

  it "has a valid factory" do
    book = attributes_for :book
    expect(book.class).to eq(Hash)
  end

  describe "#new" do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:author) }
    it { should validate_presence_of(:year) }
    it { should validate_presence_of(:cover) }
    it { should validate_presence_of(:publisher) }
    it { should validate_presence_of(:isbn) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:category_id) }
    it { should validate_presence_of(:type_id) }
  end

  it { should belong_to :category }
  it { should belong_to :type }

end
