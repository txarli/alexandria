# == Schema Information
#
# Table name: types
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Type, type: :model do

  it "has a valid factory" do
    type = attributes_for :type
    expect(type.class).to eq(Hash)
  end

  describe "#new" do
    it { should validate_presence_of(:title) }
  end

  it { should have_many(:books) }

end
