# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Category, type: :model do

  it "has a valid factory" do
    category = attributes_for :category
    expect(category.class).to eq(Hash)
  end

  describe "#new" do
    it { should validate_presence_of(:title) }
  end

  it { should have_many(:books) }

end
