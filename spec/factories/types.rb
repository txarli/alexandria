# == Schema Information
#
# Table name: types
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :type do
    title "Type name"
  end
end
