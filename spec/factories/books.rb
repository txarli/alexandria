# == Schema Information
#
# Table name: books
#
#  id          :integer          not null, primary key
#  title       :string
#  author      :string
#  year        :datetime
#  cover       :string
#  publisher   :string
#  isbn        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#  type_id     :integer
#

FactoryGirl.define do
  factory :book do
    title "Foo"
    author "Bar"
    year "2016-02-07 17:11:14"
    cover "random.jpg"
    publisher "JohnDoe"
    isbn "XX-0001122233311"
    description "Loren ipsum..."
  end
end
